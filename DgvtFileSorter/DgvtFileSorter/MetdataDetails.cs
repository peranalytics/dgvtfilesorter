﻿using System;
using Newtonsoft.Json;

namespace DgvtFileSorter
{
    public class MetadataDetails
    {
        [JsonProperty(PropertyName = "psb_number")]
        public int RelevantAuthority { get; set; }

        [JsonProperty(PropertyName = "file_name")]
        public string FileName { get; set; }

        [JsonProperty(PropertyName = "Year")]
        public string Period { get; set; }

        [JsonProperty(PropertyName = "uploaded_by")]
        public string UploadedBy { get; set; }

        [JsonProperty(PropertyName = "type")]
        public string FileType { get; set; }

        [JsonProperty(PropertyName = "num_records")]
        public int RecordCount { get; set; }

        [JsonProperty(PropertyName = "scheme_file")]
        public string SchemeFile { get; set; }

        public SchemeType SchemeType { get; set; }

        public DateTime Created { get; set; }

        public DateTime Modified { get; set; }

        public bool Validated { get; set; }
    }
}
