﻿using System.Configuration;

namespace DgvtFileSorter
{
    public static class GlobalSettings
    {
        public static string FileFolder => ConfigurationManager.AppSettings["FileFolder"];

        public static string Import => ConfigurationManager.AppSettings["ImportFolder"];

        public static string ProgramFolder => ConfigurationManager.AppSettings["ProgramFolder"];

        public static string SPS => ConfigurationManager.AppSettings["SPS"];

        public static string AccruedLiability => ConfigurationManager.AppSettings["AccruedLiability"];

        public static string Unknown => ConfigurationManager.AppSettings["Unknown"];
    }
}
