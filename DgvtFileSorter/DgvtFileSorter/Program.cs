﻿using System;
using System.IO;
using System.Linq;
using System.Security.AccessControl;
using Newtonsoft.Json;

namespace DgvtFileSorter
{
    public class Program
    {
        private static string _fileFolder;

        public static void Main(string[] args)
        {
            LoadFiles();
        }

        private static void LoadFiles()
        {
            _fileFolder = GlobalSettings.FileFolder;

            //ClearOutSchemeFolders();
            LoadMetaFiles(_fileFolder);
        }

        private static void ClearOutSchemeFolders()
        {
            var info = new DirectoryInfo(_fileFolder);
            var directories = info.GetDirectories();
            var importDirectoryName = GlobalSettings.Import;
            var programDirectoryName = GlobalSettings.ProgramFolder;

            foreach (var directoryInfo in directories)
            {
                if (directoryInfo.Name != importDirectoryName && directoryInfo.Name != programDirectoryName)
                {
                    if (directoryInfo.GetFiles().Any())
                    {
                        Console.WriteLine("directoryInfo.GetFiles()");

                        foreach (var fileInfo in directoryInfo.GetFiles())
                        {
                            Console.WriteLine("fileInfo" + fileInfo.FullName);
                            fileInfo.Delete();
                        }
                    }
                }
            }
        }

        public static void LoadMetaFiles(string schemePath)
        {
            var importDirectory = $"{_fileFolder}{GlobalSettings.Import}";
            var info = new DirectoryInfo(importDirectory);
            var files = info.GetFiles().Where(x => x.Extension == ".json").OrderBy(x => x.CreationTime).ToList();

            if (!files.Any())
            {
                return;
            }

            foreach (var file in files)
            {
                ProcessMetaFile(file);
            }
        }

        private static void ProcessMetaFile(FileSystemInfo file)
        {
            var stream = File.ReadAllText(file.FullName);

            var result = JsonConvert.DeserializeObject<Metadata>(stream);
            var resultDetails = JsonConvert.DeserializeObject<MetadataDetails>(stream);
            string schemeFolder;

            result.MetadataFileName = file.FullName;

            switch (resultDetails.SchemeFile)
            {
                case "SPS Benefits Payment":
                    resultDetails.SchemeType = SchemeType.Benefits;
                    schemeFolder = GlobalSettings.SPS;
                    break;
                case "SPS Membership Data":
                    resultDetails.SchemeType = SchemeType.Members;
                    schemeFolder = GlobalSettings.SPS;
                    break;
                case "Accrued Liability - Deferreds":
                    resultDetails.SchemeType = SchemeType.AccruedLiabilityDeferred;
                    schemeFolder = GlobalSettings.AccruedLiability;
                    break;
                case "Accrued Liability - Actives":
                    resultDetails.SchemeType = SchemeType.AccruedLiabilityActives;
                    schemeFolder = GlobalSettings.AccruedLiability;
                    break;
                case "Accrued Liability - Payment":
                    resultDetails.SchemeType = SchemeType.AccruedLiabilitPayments;
                    schemeFolder = GlobalSettings.AccruedLiability;
                    break;
                default:
                    resultDetails.SchemeType = SchemeType.Unknown;
                    schemeFolder = GlobalSettings.Unknown;
                    break;
            }

            var schemePath = $"{_fileFolder}{schemeFolder}";
            var importPath = $"{_fileFolder}{GlobalSettings.Import}";

            var newMetaPath = $"{schemePath}{file.Name}.{file.Extension}";

            var oldFilePath = $"{importPath}\\{resultDetails.FileName}";
            var newFilePath = $"{schemePath}{resultDetails.FileName}";

            File.Move(file.FullName, newMetaPath);
            File.Move(oldFilePath, newFilePath);
        }
    }
}
