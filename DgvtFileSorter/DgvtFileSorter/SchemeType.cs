﻿namespace DgvtFileSorter
{
    public enum SchemeType
    {
        Members = 1,
        Benefits = 2,
        AccruedLiabilityActives = 3,
        AccruedLiabilityDeferred = 4,
        AccruedLiabilitPayments = 5,
        Unknown = 6
    }
}
